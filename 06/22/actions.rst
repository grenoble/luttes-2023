.. index::
   ! Vive l'écologie radicale

.. _actions_2023_06_22:

=========================================================================================================================================
**Actions du jeudi 22 juin 2023 #Grenoble #SoulevementsDeLaTerre**  |sdterre| ⏚ #SoulevementsDeLaTerre #SLT ⏚ #FossilFascism
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html

#2023-06-20 #Grenoble #SoulevementsDeLaTerre



|ici_grenoble|  ⏚ A la une **Vive l'écologie radicale** ⏚
==================================================================================

- https://www.ici-grenoble.org/article/vive-lecologie-radicale
- https://lessoulevementsdelaterre.org/blog/la-carte-des-rassemblements-de-soutien-aux-inculpe.es-et-contre-la-dissolution


.. figure:: images/page_une_ici_grenoble_2023_06_22.png
   :align: center

   https://www.ici-grenoble.org/article/vive-lecologie-radicale


La dissolution des Soulèvements de la Terre vous révolte ?

Face au désastre climatique, vous pensez qu'il ne reste que deux
scénarios possibles : soit l'aggravation, soit le renversement du
système capitaliste ?

Mercredi 28 juin à 18h, le comité grenoblois des Soulèvements de la Terre
organise une manifestation devant la Préfecture de l'Isère.

Voici l'appel :

"La première menace de dissolution de Gérald Darmanin le 28 mars 2023 a
provoqué une vague de soutiens inédite : plus de 100 000 personnes se sont
revendiquées des Soulèvements de la Terre dans un appel qui continue
de se renforcer.

Dans le même temps, partout en France et à l'étranger ont fleuri plus de
170 comités locaux.

Aujourd'hui, mercredi 21 juin, le gouvernement prononce la dissolution
effective des Soulèvements de la Terre, que nous dénonçons comme une
attaque grave contre les libertés fondamentales et la démocratie.

Cette dissolution marque un précédent dangereux, un point de non-retour
dans la dérive autoritaire du gouvernement.

Décrié par l'ONU pour son maintien de l'ordre brutal, ses violences policières,
sa criminalisation du militantisme écologique, le gouvernement a pour
seule réponse une fuite en avant répressive.

Après avoir court-circuité la représentation nationale à coup de 49.3 et
ignoré des millions de manifestants lors de sa réforme des retraites,
il rend illégal un mouvement soutenu par une centaine de milliers de
citoyennes et citoyens.

Alors que l'urgence climatique et écologique se fait chaque jour plus
pressante, que les sécheresses et incendies s'intensifient, que la
biodiversité s'effondre, le gouvernement poursuit son œuvre écocidaire,
au service des lobbies du BTP, de l'agrochimie et de la finance.

En témoigne l'appel d'Emmanuel Macron à une « pause réglementaire sur les
normes environnementales européennes », comme si la catastrophe pouvait
attendre.

Ces manœuvres sont celles d’un vieux monde, condamné mais incapable de
changer de direction.

Face à un capitalisme frénétique dans son accaparement de la terre, de
l'eau et des ressources, les mouvements de défense du vivant sont taxés
d'écoterroristes.
La criminalisation des oppositions n'est que la stratégie classique
d'un pouvoir aux abois.

Ce que le gouvernement feint d'ignorer, c’est que la dissolution des
Soulèvements de la Terre ne suffira pas.
Un mouvement qui se bat pour la protection des biens communs que sont
l'eau et la terre, ne peut être dissout.

Partout nous refleurirons car la répression brutale imposée par le
gouvernement reste toujours moins effrayante que l'avenir suffoquant
qu'il nous réserve.

Soyons nombreuses et nombreux à manifester notre soutien aux Soulèvements
de la Terre, rejoignons les appels des comités locaux, et notamment
celui de Grenoble !

Donnons-nous rendez-vous collectivement la semaine prochaine,
mercredi 28 juin 2023 à 18h devant la préfecture de l’Isère, place de
Verdun pour dénoncer cette dangereuse dissolution par un soulèvement festif !"

Contact : sdt-grenoble@proton.me


**Agenda ici-grenoble**
==========================

.. figure::  images/ici_grenoble_2023_06_22.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 10h00 https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- 12h00 https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- 14h00 https://www.ici-grenoble.org/evenement/atelier-couture-aux-petites-cantines
- 14h30 https://www.ici-grenoble.org/evenement/permanence-dautodefense-pour-lacces-aux-droits-caf-secu-titres-de-sejour-dossiers-a-remplir
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- 18h00 https://www.ici-grenoble.org/evenement/atelier-de-francais-a-prix-libre-pour-debutant-es-et-intermediaires
- |important| 18h00 https://www.ici-grenoble.org/evenement/rassemblement-verite-et-justice-pour-alhoussein-camara-tue-le-14-juin-par-un-policier
- 18h30 https://www.ici-grenoble.org/evenement/lecture-debat-insurrections-comment-vaincre-la-tyrannie-du-et-apres

  - https://lvsl.fr/insurrections-comment-vaincre-la-tyrannie-du-et-apres/

- 19h00 https://www.ici-grenoble.org/evenement/atelier-la-fresque-du-sexisme-detricoter-la-mecanique-sexiste-construire-une-societe-egalitaire

  - https://www.billetweb.fr/animation-la-fresque-du-sexisme-presentiel-grenoble

- 20h00 https://www.ici-grenoble.org/evenement/decouverte-du-jeu-de-societe-cooperatif-autonomia-zapatista

  - https://boardgamegeek.com/boardgame/337636/autonomia-zapatista

- 20h15 https://www.ici-grenoble.org/evenement/cine-debat-yallah-gaza-la-bande-de-gaza-vue-par-les-gazaoui-e-s
