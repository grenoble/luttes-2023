
.. _actions_2023_06_09:

=========================================================================================================================================
**Actions du vendredi 9 juin 2023 #Grenoble #Solidarite**
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html


#2023-06-09 #Grenoble #Solidarite


|ici_grenoble| A la une **Les 12 infos de juin**
==================================================================================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-12-infos-de-juin

.. figure:: ../08/images/page_une_ici_grenoble_2023_06_08.png
   :align: center

   https://www.ici-grenoble.org/article/grevue-de-presse-les-12-infos-de-juin, https://www.ici-grenoble.org/newsletter


Vous ne lisez pas le Dauphiné Libéré, mais les infos marquantes vous intéressent ?

Chaque mois, le média ici Grenoble vous propose la `Grevue de presse <https://www.ici-grenoble.org/article/grevue-de-presse-les-12-infos-de-juin>`_,
une sélection d'infos courtes et percutantes glanées dans la presse locale
ou dans nos réseaux.

Au menu de juin : un gendarme néonazi en Isère, la clinique mutualiste
sous tutelle, un congé menstruel à Seyssinet, des Cacatovs à Grenoble,
le relogement de Solidarité Femmes Miléna, un appel du Tichodrome, la
nappe toxique sous Grenoble...

Bonne lecture !


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_09.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 09h59 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- |important| 12h00 https://www.ici-grenoble.org/evenement/assemblee-docccupation-pour-la-requisition-des-logements-vides-a-grenoble
- https://www.ici-grenoble.org/evenement/fresque-des-frontieres-planetaires (en erreur 500 ?)
- 15h00 https://www.ici-grenoble.org/evenement/permanence-du-lokal-autogere
- 16h00 https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- 16h10 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 17h40 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- |important| |car38| 19h30 https://www.ici-grenoble.org/evenement/boum-antirepression-en-soutien-notamment-aux-interpelles-du-1er-mai
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
- 21h30 🎥 https://www.ici-grenoble.org/evenement/cinema-marinette-sur-la-footballeuse-lesbienne-marinette-pichon-une-heroine-meconnue

Demosphere 38
=================

.. figure:: images/demo_2023_06_09.png
   :align: center

   https://38.demosphere.net/


- 19h30 https://38.demosphere.net/rv/1199 (Boum de soutien à la répression du mouvement social, 38 rue d'Alembert)


- https://cric-grenoble.info/infos-locales/article/soiree-de-soutien-a-la-repression-du-mouvement-social-09-07-3051 |car38|
