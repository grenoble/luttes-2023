
.. _actions_2023_06_15:

=========================================================================================================================================
**Actions du jeudi 15 juin 2023 #Grenoble #LyonTurin #SoulevementsDeLaTerre #NoTAV #NoTAVaran #AnalyseRetraites**
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html

#2023-06-15 #Grenoble #LyonTurin #SoulevementsDeLaTerre #NoTAV #NoTAVaran #Analyse


|ici_grenoble| A la une **No TAVARAN**
==================================================================================

- https://www.ici-grenoble.org/article/no-tavaran
- https://librinfo74.fr/du-16-au-18-juin-manifestation-de-force-pacifique-contre-le-lyon-turin/

.. figure:: images/page_une_ici_grenoble_2023_06_15.png
   :align: center

   https://www.ici-grenoble.org/article/no-tavaran, https://www.ici-grenoble.org/newsletter



**Agenda ici-grenoble**
==========================

.. figure::  images/ici_grenoble_2023_06_15.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 09h59 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 12h00 https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- 14h30 https://www.ici-grenoble.org/evenement/permanence-dautodefense-pour-lacces-aux-droits-caf-secu-titres-de-sejour-dossiers-a-remplir
- |important| 🎥 16h20 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- 17h40 🎥 https://www.ici-grenoble.org/evenement/cinema-marinette-sur-la-footballeuse-lesbienne-marinette-pichon-une-heroine-meconnue

  - https://www.change.org/p/diffusez-les-matchs-des-bleues-%C3%A0-la-t%C3%A9l%C3%A9 (Lancée par Marinette PICHON
    À moins de deux mois du coup d’envoi, la Coupe du monde de foot Dames n’a toujours pas de diffuseur dans l’Hexagone.
    Il serait scandaleux que les matchs des Bleues ne soient pas retransmis à la télévision française)


- 18h00 https://www.ici-grenoble.org/evenement/atelier-de-francais-a-prix-libre-pour-debutant-es-et-intermediaires
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
- |important| 22h00 https://www.ici-grenoble.org/evenement/special-retraites-pourquoi-lintersyndicale-a-echoue-jusquici-selon-frederic-lordon

  - http://basta.media/Reforme-des-retraites-les-huit-arguments-fallacieux-du-gouvernement-pour-reculer-l-age-de-depart
  - https://blog.mondediplo.net/vouloir-perdre-vouloir-gagner

2023-06-15 18h00 conférence « Désobéissance civile et civique. Le bras de fer des militants écologistes dans le prétoire »
=============================================================================================================================

15 juin 2023, 18h00 à la Maison des Avocats de Grenoble, conférence **Désobéissance civile
et civique. Le bras de fer des militants écologistes dans le prétoire**

Avec Serge SLAMA , professeur de droit public à l’UGA, Pierre JANOT et
Elsa GHANASSIA, avocats du Barrerau de Grenoble.

Partenariat LDH Grenoble-Métropole et Barreau de Grenoble.

Formulé dans un premier temps « Violence légitime / violence illégitime »,
le thème s’est concrétisé sur la formule actuelle.


.. _analyse_retraites_2023_06_15:

Spécial retraites : Pourquoi l'intersyndicale a échoué jusqu'ici (selon Frédéric Lordon)
===============================================================================================

Malgré le soutien d'une majorité de Français-es, pourquoi la lutte `contre
une réforme des retraites injuste <Spécial retraites : Pourquoi l'intersyndicale a échoué jusqu'ici (selon Frédéric Lordon)>`_ a-t-elle échoué jusqu'ici ?

L'intersyndicale a-t-elle commis des erreurs ? Lesquelles ?

Voici l'analyse du sociologue Frédéric Lordon, publiée dans `Vouloir perdre, vouloir gagner <https://blog.mondediplo.net/vouloir-perdre-vouloir-gagner>`_:

"Quand un pouvoir en est à redouter des casseroles, des bouts de papier
rouges et des sifflets, c’est qu’il est au bord de tomber.

Est-on fondé à se dire. Et pourtant il tient.

Il tient parce que des institutions totalement vicieuses le lui permettent.
Parce que toute moralité politique, tout ethos démocratique, l’ont abandonné.
Parce qu’il est aux mains de forcenés qui n’ont plus aucune idée de limite.

Il tient aussi parce que les conducteurs du mouvement – pour parler clair,
l’Intersyndicale – n’ont pas eu le début du commencement d’une analyse
de l’adversaire, et persistent dans une stratégie désormais avérée
perdante – on n’avait d’ailleurs nul besoin de passer quatre mois à le
vérifier : on pouvait le leur dire dès le premier jour.

Les stratégies de la décence démocratique, par la seule manifestation
paisible du nombre, échouent là où, en face, il n’y a plus que de
l’indécence démocratique.

Comme il était clair depuis ce même premier jour à qui n’avait pas envie
de se raconter des histoires pour enfants, l’Intersyndicale était partie
pour perdre.

C’est bien ce qui l’a rendue si sympathique aux yeux des médias.

Quand elle se penche sur les forces de gauche, la presse bourgeoise n’a
d’yeux et de sentiment que pour celles qui sont de droite ou pour celles
qui sont perdantes.

C’est une loi absolument générale que la presse bourgeoise est une
instance de consécration négative.

La chose vaut en matière de littérature, de pensée, d’art, comme en
politique : ceux que la presse bourgeoise bénit, par là on connaît leur
« valeur », et aussi leur destin — entre innocuité, phagocytose et
renégation.

La condition nécessaire de l’espoir, d’une perspective, c’est de se
diriger vers ceux qu’elle exècre."

Pour lire la suite, c'est `ici <https://blog.mondediplo.net/vouloir-perdre-vouloir-gagner>`_
(https://blog.mondediplo.net/vouloir-perdre-vouloir-gagner).



