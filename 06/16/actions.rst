
.. _actions_2023_06_16:

=========================================================================================================================================
**Actions du vendredi 16 juin 2023 #Grenoble #LyonTurin #SoulevementsDeLaTerre #NoTAV #NoTAVaran**
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html

#2023-06-16 #Grenoble #LyonTurin #SoulevementsDeLaTerre #NoTAV #NoTAVaran


|ici_grenoble| A la une **No TAVARAN**
==================================================================================

- https://www.ici-grenoble.org/article/no-tavaran
- https://librinfo74.fr/du-16-au-18-juin-manifestation-de-force-pacifique-contre-le-lyon-turin/

.. figure:: ../15/images/page_une_ici_grenoble_2023_06_15.png
   :align: center

   https://www.ici-grenoble.org/article/no-tavaran, https://www.ici-grenoble.org/newsletter



**Agenda ici-grenoble**
==========================

.. figure::  images/ici_grenoble_2023_06_16.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 09h59 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 10h00 https://www.ici-grenoble.org/evenement/paillettes-vener-festival-trans-bi-gouine-ateliers-concerts-films

  - https://gouinefestgrenoble.wixsite.com/paillettesvener

- 14h30 https://www.ici-grenoble.org/evenement/repair-cafe-de-grenoble-centre *
- de 16h à 20h00 https://www.ici-grenoble.org/evenement/la-cyclique-permanence-de-mecanique-velo-en-mixite-choisie-sans-hommes-cisgenres
- 16h00 https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- 17h40 🎥 https://www.ici-grenoble.org/evenement/cinema-marinette-sur-la-footballeuse-lesbienne-marinette-pichon-une-heroine-meconnue

  - https://www.change.org/p/diffusez-les-matchs-des-bleues-%C3%A0-la-t%C3%A9l%C3%A9 (Lancée par Marinette PICHON
    À moins de deux mois du coup d’envoi, la Coupe du monde de foot Dames n’a toujours pas de diffuseur dans l’Hexagone.
    Il serait scandaleux que les matchs des Bleues ne soient pas retransmis à la télévision française)
  - https://www.change.org/p/diffusez-les-matchs-des-bleues-%C3%A0-la-t%C3%A9l%C3%A9/u/31677918
    (C'est officiel, France TV et M6 ont acquis les droits de la Coupe du monde féminine ! Votre mobilisation y est pour beaucoup !)

- 20h50 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques

