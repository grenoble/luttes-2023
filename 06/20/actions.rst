
.. _actions_2023_06_20:

=========================================================================================================================================
**Actions du mardi 20 juin 2023 #Grenoble #DefenseDesHopitaux #ServicePublic #AG**
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html

#2023-06-20 #Grenoble #DefenseDesHopitaux #ServicePublic



**Agenda ici-grenoble**
==========================

.. figure::  images/ici_grenoble_2023_06_20.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 09h56 https://www.ici-grenoble.org/evenement/proces-waffenkraft-impliquant-un-gendarme-neonazi-iserois
- 09h59 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 10h00 https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- 12h00 https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- 14h00 https://www.ici-grenoble.org/evenement/repair-cafe-de-grenoble-pinal-pres-de-la-gare
- |important| 14h00 https://www.ici-grenoble.org/evenement/des-bras-et-des-lits-rassemblement-de-soutien-aux-hopitaux-et-au-service-public-de-sante
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement-en-tant-que-locataires
- |important| 18h30 https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- 19h00 https://www.ici-grenoble.org/evenement/soiree-transfem-entre-meufs-trans-personnes-inter-ou-non-binaires-amab-travesti-es-ou-en-questionnement
