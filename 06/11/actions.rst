
.. _actions_2023_06_11:

=========================================================================================================================================
**Actions du dimanche 11 juin 2023 #Grenoble #LyonTurin #SoulevementsDeLaTerre #NoTAV**  |sdterre|
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html

#2023-06-11 #Grenoble #LyonTurin #SoulevementsDeLaTerre #NoTAV


|ici_grenoble| A la une **Contre le Lyon-Turin** |sdterre|
==================================================================================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-12-infos-de-juin

.. figure:: images/page_une_ici_grenoble_2023_06_11.png
   :align: center

   https://www.ici-grenoble.org/article/contre-le-lyon-turin, https://www.ici-grenoble.org/newsletter


Du 16 au 18 juin 2023 dans la vallée de la Maurienne, une mobilisation
internationale s'organise contre l'immense projet de tunnel ferroviaire
Lyon-Turin, avec une manifestation à 10h le samedi 17 juin 2023.

Le `comité grenoblois des Soulèvements de la Terre <https://lessoulevementsdelaterre.org/>`_
(https://lessoulevementsdelaterre.org/) y sera.

Pour toute question : sdt-grenoble@proton.me

Voici l'appel à se mobiliser
--------------------------------

"Depuis des dizaines d'années, des deux côtés des Alpes, en France comme
en Italie, collectifs et associations se mobilisent pour qu'un projet
pharaonique, inutile et désastreux ne voit jamais le bout du tunnel.

Ce projet, c'est la seconde ligne ferroviaire Lyon Turin : 30 milliards
d'euros pour 270 km de dévastation, en surface et à travers de multiples
galeries sous nos montagnes.

Le tunnel transfrontalier représente à lui seul 2 tubes de 57,5 km chacun !

**Les conséquences ?**

1500 hectares de zones agricoles et naturelles à artificialiser, des
millions de tonnes de déchets issues des galeries à stocker, le drainage
de 100 millions de m3 souterraine chaque année à prévoir, asséchant de
façon irrémédiable la montagne.

**Si l'eau c'est la vie, alors c'est bien au droit à vivre des populations
locales que ce projet s'attaque...**

Faire transiter les marchandises par le rail plutôt que par la route,
pourtant, c'est bien écologique ?
Certainement. Sauf qu'il existe déjà une ligne, fortement sous utilisée,
sur laquelle le fret ferroviaire s'est effondré : 10 millions de tonnes
transportées en 1993, 3,3 millions en 2021.
Et ce malgré des travaux conséquents de mise aux normes !

**Et le climat, alors ?**

L'impact des travaux est tel qu'il faudra des dizaines d'années pour
espérer compenser la dette carbone qui est creusée en ce moment même
(selon la Cour des comptes européenne il faudra probablement attendre
jusqu'en... 2085 !).
Alors que tout le monde s'accorde sur l'urgence climatique et le besoin
d'agir immédiatement, le LYON-TURIN participe activement au réchauffement
climatique.

Symbole d'une époque où l'on ne jurait que par l'explosion du trafic de
marchandises et la grande vitesse, ce désastre environnemental a démarré
(11km creusés sur les 115 nécessaires pour le tunnel transfrontalier),
mais il est encore possible d'éviter le pire en faisant dérailler ce
projet écocide."

Pour prendre conscience des enjeux et du désastre environnemental que
représente ce projet, nous vous recommandons ce reportage : `Le 17 Juin, la montagne se soulève <https://www.youtube.com/watch?v=A6j8unxb0xs>`_
(https://www.youtube.com/watch?v=A6j8unxb0xs).

`Plus d'infos ici <https://stopaulyonturin.com/>`_ (https://stopaulyonturin.com/)


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_11.png
   :align: center

   https://www.ici-grenoble.org/agenda

- fin 06h30 https://www.ici-grenoble.org/evenement/glitter-bloc-nuit-de-concerts-pour-les-4-ans-de-lassociation-serein-e-s
- 12h00 https://www.ici-grenoble.org/evenement/le-br-br-de-deviations-brosses-et-brunch-entre-personnes-deviantes-de-genre
- 17h10 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 17h50 🎥 https://www.ici-grenoble.org/evenement/cinema-marinette-sur-la-footballeuse-lesbienne-marinette-pichon-une-heroine-meconnue
- 18h00 |car38| https://www.ici-grenoble.org/evenement/permanence-du-collectif-anti-repression-grenoble-soutien-aux-victimes-de-violences-policieres
- 18h25 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques






