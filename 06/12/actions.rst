
.. _actions_2023_06_12:

=========================================================================================================================================
**Actions du lundi 12 juin 2023 #Grenoble #LyonTurin #SoulevementsDeLaTerre #NoTAV #LogicielLibre #OSM #OpenStreetMap**
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html

#2023-06-12 #Grenoble #LyonTurin #SoulevementsDeLaTerre #NoTAV #LogicielLibre #OSM #OpenStreetMap


|ici_grenoble| A la une **Contre le Lyon-Turin**
==================================================================================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-12-infos-de-juin

.. figure:: ../11/images/page_une_ici_grenoble_2023_06_11.png
   :align: center

   https://www.ici-grenoble.org/article/contre-le-lyon-turin, https://www.ici-grenoble.org/newsletter



Du 16 au 18 juin 2023 dans la vallée de la Maurienne, une mobilisation
internationale s'organise contre l'immense projet de tunnel ferroviaire
Lyon-Turin, avec une manifestation à 10h le samedi 17 juin 2023.

Le `comité grenoblois des Soulèvements de la Terre <https://lessoulevementsdelaterre.org/>`_
(https://lessoulevementsdelaterre.org/) y sera.

Pour toute question : sdt-grenoble@proton.me

Voici l'appel à se mobiliser
--------------------------------

"Depuis des dizaines d'années, des deux côtés des Alpes, en France comme
en Italie, collectifs et associations se mobilisent pour qu'un projet
pharaonique, inutile et désastreux ne voit jamais le bout du tunnel.

Ce projet, c'est la seconde ligne ferroviaire Lyon Turin : 30 milliards
d'euros pour 270 km de dévastation, en surface et à travers de multiples
galeries sous nos montagnes.

Le tunnel transfrontalier représente à lui seul 2 tubes de 57,5 km chacun !

**Les conséquences ?**

1500 hectares de zones agricoles et naturelles à artificialiser, des
millions de tonnes de déchets issues des galeries à stocker, le drainage
de 100 millions de m3 souterraine chaque année à prévoir, asséchant de
façon irrémédiable la montagne.

**Si l'eau c'est la vie, alors c'est bien au droit à vivre des populations
locales que ce projet s'attaque...**

Faire transiter les marchandises par le rail plutôt que par la route,
pourtant, c'est bien écologique ?
Certainement. Sauf qu'il existe déjà une ligne, fortement sous utilisée,
sur laquelle le fret ferroviaire s'est effondré : 10 millions de tonnes
transportées en 1993, 3,3 millions en 2021.
Et ce malgré des travaux conséquents de mise aux normes !

**Et le climat, alors ?**

L'impact des travaux est tel qu'il faudra des dizaines d'années pour
espérer compenser la dette carbone qui est creusée en ce moment même
(selon la Cour des comptes européenne il faudra probablement attendre
jusqu'en... 2085 !).
Alors que tout le monde s'accorde sur l'urgence climatique et le besoin
d'agir immédiatement, le LYON-TURIN participe activement au réchauffement
climatique.

Symbole d'une époque où l'on ne jurait que par l'explosion du trafic de
marchandises et la grande vitesse, ce désastre environnemental a démarré
(11km creusés sur les 115 nécessaires pour le tunnel transfrontalier),
mais il est encore possible d'éviter le pire en faisant dérailler ce
projet écocide."

Pour prendre conscience des enjeux et du désastre environnemental que
représente ce projet, nous vous recommandons ce reportage : `Le 17 Juin, la montagne se soulève <https://www.youtube.com/watch?v=A6j8unxb0xs>`_
(https://www.youtube.com/watch?v=A6j8unxb0xs).

`Plus d'infos ici <https://stopaulyonturin.com/>`_ (https://stopaulyonturin.com/)


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_12.png
   :align: center

   https://www.ici-grenoble.org/agenda

- |solidarite| https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 16h20 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 18h30 https://www.ici-grenoble.org/evenement/lecture-debat-la-tyrannie-de-labsence-de-structure-une-critique-du-spontaneisme-dans-les-collectifs-militants
- https://www.ici-grenoble.org/evenement/les-soirees-denjeux-communs-le-12-juin-2023
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
- |important| 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 21h30 🎥 https://www.ici-grenoble.org/evenement/cinema-marinette-sur-la-footballeuse-lesbienne-marinette-pichon-une-heroine-meconnue


|tamis| Événements du Tamis
================================

.. figure:: images/tamis_2023_06_12.png
   :align: center

   https://www.le-tamis.info/evenements

- de 14h00 à 19h00 https://www.le-tamis.info/evenement/permanence-du-garage-associatif-les-soupapes2
- de 17h30 à 20h30 https://www.le-tamis.info/evenement/biliotheque-feministe-en-non-mixite
- de 19h00 à 21h00 https://www.le-tamis.info/evenement/latelier-de-bidouille-informatique-libre-abil-1


|osm| de 19h à 21h Réunion Openstreemap Grenoble
=======================================================

- https://wiki.openstreetmap.org/wiki/Grenoble_groupe_local/Agenda#Lundi_12_juin_Atelier_%22Auberge_espagnole%22
- https://turbine.coop/evenement/atelier-openstreetmap-juin-2023/


C’est quoi OSM (OpenStreetMap) ?
-------------------------------------

OpenStreetMap (OSM) est un projet international fondé en 2004 dans le but
de créer une carte libre du monde.

Nous collectons des données dans le monde entier sur les routes, voies
ferrées, les rivières, les forêts, les bâtiments et bien plus encore !

Les données cartographiques collectées sont ré-utilisables sous licence
libre ODbL (depuis le 12 septembre 2012).

Pour plus d’information : inscrivez-vous à la `liste locale OSM de Grenoble <http://listes.openstreetmap.fr/wws/info/local-grenoble>`_
(http://listes.openstreetmap.fr/wws/info/local-grenoble)


Liens |osm| **Openstreetmap** |sotm|
--------------------------------------------

- :ref:`osm:openstreetmap`
- :ref:`osm_2023:sotmfr_2023`



Première partie : planification des actions futures
-------------------------------------------------------

- Cartopartie
- Thématique vélo pour la saison prochaine ?

Seconde partie : venez présenter rapidement un sujet OSM que vous avez découvert
===================================================================================

Seconde partie : venez présenter rapidement un sujet OSM que vous avez découvert,

sur lequel vous avez contribué, ou pour lequel vous aimeriez constituer
un groupe de contribution. Un PC branché sur un vidéoprojecteur sera disponible.

Informations pratiques
-----------------------------

**Atelier ouvert à tous et à toutes**

Inscription souhaitée sur la `page de l'événement <https://framaforms.org/atelier-openstreetmap-juin-2023-1684399149>`_

**La Turbine.coop, 5 Esplanade Andry Farcy, 38000 Grenoble**

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.701523423194886%2C45.18619972617592%2C5.7086044549942025%2C45.189296173804365&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=18/45.18775/5.70506">Afficher une carte plus grande</a></small>



