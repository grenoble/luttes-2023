.. index::
   ! Vive l'écologie radicale

.. _actions_2023_06_28:

=========================================================================================================================================
|sdterre| **Actions du mercredi 28 juin 2023 #Grenoble #Solidarité #SoulevementsDeLaTerre**
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html

#2023-06-28 #Grenoble #Solidarité #SoulevementsDeLaTerre



|ici_grenoble|  A la une **Alors on changera de nom**
==================================================================================

- https://www.ici-grenoble.org/article/alors-on-changera-de-nom
- https://lessoulevementsdelaterre.org/
- https://lundi.am/Appel-international-de-soutien-aux-Soulevements-de-la-Terre
- https://lessoulevementsdelaterre.org/blog/la-carte-des-rassemblements-de-soutien-aux-inculpe.es-et-contre-la-dissolution
- https://t.me/infoline_25mars/, https://web.telegram.im/#@infoline_25mars
- https://umap.openstreetmap.fr/fr/map/21-juin-28-juin-rassemblements-contre-la-repressio_931019#8/45.683/6.493


.. figure:: images/page_une_ici_grenoble_2023_06_28.png
   :align: center

   https://www.ici-grenoble.org/article/alors-on-changera-de-nom


Introduction
-----------------

**Le désastre climatique s'approfondit, l'écologie radicale monte en puissance,
le gouvernement diabolise et réprime, mais ce n'est qu'un début**.

Mercredi 28 juin 2023 à 18h, le comité grenoblois des Soulèvements de la
Terre organise un rassemblement festif devant la Préfecture de l'Isère,
contre la dissolution autoritaire du mouvement.

Face au désastre climatique, vous pensez qu'il ne reste que deux scénarios
possibles : soit l'aggravation, soit le renversement du système capitaliste ?

Voici l'appel à manifester
---------------------------------

"La première menace de dissolution de Gérald Darmanin le 28 mars dernier
a provoqué une vague de soutiens inédite : plus de 100 000 personnes se
sont revendiquées des Soulèvements de la Terre dans un appel qui continue
de se renforcer.

Dans le même temps, partout en France et à l'étranger ont fleuri plus de
170 comités locaux.

Aujourd'hui, mercredi 21 juin 2023, le gouvernement prononce la dissolution
effective des Soulèvements de la Terre, que nous dénonçons comme une
attaque grave contre les libertés fondamentales et la démocratie.

Cette dissolution marque un précédent dangereux, un point de non-retour
dans la dérive autoritaire du gouvernement.
Décrié par l'ONU pour son maintien de l'ordre brutal, ses violences policières,
sa criminalisation du militantisme écologique, le gouvernement a pour
seule réponse une fuite en avant répressive.

Après avoir court-circuité la représentation nationale à coup de 49.3 et
ignoré des millions de manifestants lors de sa réforme des retraites,
il rend illégal un mouvement soutenu par une centaine de milliers de
citoyennes et citoyens.

Alors que l'urgence climatique et écologique se fait chaque jour plus
pressante, que les sécheresses et incendies s'intensifient, que la
biodiversité s'effondre, le gouvernement poursuit son œuvre écocidaire,
au service des lobbies du BTP, de l'agrochimie et de la finance.

En témoigne l'appel d'Emmanuel Macron à une « pause réglementaire sur les
normes environnementales européennes », comme si la catastrophe pouvait
attendre. Ces manœuvres sont celles d’un vieux monde, condamné mais
incapable de changer de direction.

Face à un capitalisme frénétique dans son accaparement de la terre, de
l'eau et des ressources, les mouvements de défense du vivant sont taxés
d'écoterroristes.
La criminalisation des oppositions n'est que la stratégie classique d'un
pouvoir aux abois.

Ce que le gouvernement feint d'ignorer, c’est que la dissolution des
Soulèvements de la Terre ne suffira pas. Un mouvement qui se bat pour la
protection des biens communs que sont l'eau et la terre, ne peut être dissout.

Partout nous refleurirons car la répression brutale imposée par le
gouvernement reste toujours moins effrayante que l'avenir suffoquant
qu'il nous réserve.

Soyons nombreuses et nombreux à manifester notre soutien aux Soulèvements
de la Terre, rejoignons les appels des comités locaux, et notamment celui
de Grenoble !

Donnons-nous rendez-vous collectivement la semaine prochaine,
mercredi 28 juin 2023 à 18h devant la préfecture de l’Isère, place de Verdun
pour dénoncer cette dangereuse dissolution par un soulèvement festif !"

Contact : sdt-grenoble@proton.me


.. figure:: images/appel_international.png
   :align: center

   https://lundi.am/Appel-international-de-soutien-aux-Soulevements-de-la-Terre


Julien le Guet, porte parole des Soulèvements de la terre
==============================================================

- https://glieres.frama.io/crha/resistances-2023/05/21/prises-de-parole/julien-le-guet/julien-le-guet.html
- https://piped.video/watch?v=zQ3z5Ee6hm0



**Agenda ici-grenoble**
==========================

.. figure::  images/ici_grenoble_2023_06_28.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 10h00 https://www.ici-grenoble.org/evenement/le-28-juin-1969-les-emeutes-de-stonewall-inn
- 16h00 https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-lokal-autogere
- |important| 18h00 https://www.ici-grenoble.org/evenement/rassemblement-festif-contre-la-dissolution-des-soulevements-de-la-terre
- 18h15 https://www.ici-grenoble.org/evenement/atelier-pourquoi-changer-de-banque-et-laquelle-choisir
- 18h30 https://www.ici-grenoble.org/evenement/cinema-marinette-sur-la-footballeuse-lesbienne-marinette-pichon-une-heroine-meconnue
- 19h00 https://www.ici-grenoble.org/evenement/apero-queer-avec-deviations
- 20h30 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques

