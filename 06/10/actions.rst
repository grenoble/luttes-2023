
.. _actions_2023_06_10:

=========================================================================================================================================
**Actions du samedi 10 juin 2023 #Grenoble #SolidariteUkraine #PlanningFamilialGrenoble #LutteAntiNazis**
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html

#2023-06-10 #Grenoble #SolidariteUkraine #PlanningFamilialGrenoble #LutteAntiNazis


|ici_grenoble| A la une **Les 12 infos de juin**
==================================================================================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-12-infos-de-juin

.. figure:: ../08/images/page_une_ici_grenoble_2023_06_08.png
   :align: center

   https://www.ici-grenoble.org/article/grevue-de-presse-les-12-infos-de-juin, https://www.ici-grenoble.org/newsletter


Vous ne lisez pas le Dauphiné Libéré, mais les infos marquantes vous intéressent ?

Chaque mois, le média ici Grenoble vous propose la `Grevue de presse <https://www.ici-grenoble.org/article/grevue-de-presse-les-12-infos-de-juin>`_,
une sélection d'infos courtes et percutantes glanées dans la presse locale
ou dans nos réseaux.

Au menu de juin : :ref:`un gendarme néonazi en Isère <gendarme_nazi_2023_06>`, la clinique mutualiste
sous tutelle, un congé menstruel à Seyssinet, des Cacatovs à Grenoble,
le relogement de Solidarité Femmes Miléna, un appel du Tichodrome, la
nappe toxique sous Grenoble...

Bonne lecture !


.. _gendarme_nazi_2023_06:

1ere info de juin 2023: **Un gendarme néonazi en isère**
----------------------------------------------------------------

- https://www.politis.fr/articles/2023/05/waffenkraft-le-projet-terroriste-dun-gendarme-neonazi/

Il s'entraînait au fusil automatique dans une forêt de Seyssinet...
Dans quelques jours, un gendarme néonazi Isérois sera jugé à Paris, dans
le cadre de `l'affaire WaffenKraft <https://www.politis.fr/articles/2023/05/waffenkraft-le-projet-terroriste-dun-gendarme-neonazi/>`_.
(https://www.politis.fr/articles/2023/05/waffenkraft-le-projet-terroriste-dun-gendarme-neonazi/)

Tout commence en 2018, lorsqu'une société de produits chimiques alerte
la police sur une commande de produits pouvant servir à l'élaboration
d'engins explosifs. Le client ? Un gendarme Isérois de 22 ans, affecté
quelques temps à Montbonnot-Saint-Martin. À son domicile, on découvre
des kalachnikovs, des munitions abondantes, un laboratoire de fabrication
de bombes.

Sous le pseudonyme FrenchCrusader, **il déversait sa haine des musulmans,
des Juifs et des "traîtres à l'Europe ancienne" sur des forums néonazis**.

Avec son groupe, il projetait d'organiser une tuerie de masse pour venger
les victimes des attentats de Paris et de Nice.

Combien y-a-t-il d'autres nénonazis en Isère ?

Parmi eux, combien sont inscrits à un `club de tir sportif <https://www.ici-grenoble.org/article/interview-des-flingues-et-des-hommes>`,
(https://www.ici-grenoble.org/article/interview-des-flingues-et-des-hommes)
comme l'était ce gendarme ?

Pour d'autres infos glaçantes sur l'affaire WaffenKraft, nous vous
recommandons un article de Politis : `Le projet terroriste d’un gendarme néonazi <https://www.politis.fr/articles/2023/05/waffenkraft-le-projet-terroriste-dun-gendarme-neonazi/>`_
(https://www.politis.fr/articles/2023/05/waffenkraft-le-projet-terroriste-dun-gendarme-neonazi/).


Cette revue de presse vous a intéressé ?
-----------------------------------------------

N'hésitez pas à consulter la `revue de presse de mai 2023 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mai>`_
(https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mai)
sur un projet de mégabassine dans le Trièves, des mégapompeurs dans le Grésivaudan,
un drone russe "made in Isère", la clinique mutualiste dans une pyramide
de Ponzi, la fin de la BAF, le début du BOCAL et plein d'autres infos.

Nous vous recommandons également la `revue de presse de mars 2023 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars>`_,
(https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars)
sur le congé menstruel à Grenoble, le point sur la ZFE, le boum du vélo, la
guerre des puces électroniques, les sabotages contre la 5G et plein
d'autres infos.

Nous vous recommandons aussi la `revue de presse de février 2023 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-fevrier>`_,
(https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-fevrier)
sur la nappe phréatique toxique sous Grenoble, le boum des maisons secondaires,
le lancement de La Nébuleuse, l'expulsion du Chantier de Fontaine, etc.

Nous vous recommandons enfin la `revue de presse de janvier 2023 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-janvier>`_,
(https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-janvier)
sur les sols dioxinés de Pont-de-Claix et de Jarrie, l'algorithme secret de
la CAF Isère, le vote du Métrocâble, les stages d'autodéfense féministe,
le bilan de 5 ans de collapsologie à Grenoble, etc.

Vous trouverez par ailleurs dans la `revue de presse de décembre 2022 <https://www.ici-grenoble.org/article/grevue-de-presse-les-8-infos-de-decembre>`_
(https://www.ici-grenoble.org/article/grevue-de-presse-les-8-infos-de-decembre)
plein d'infos sur la nouvelle Maison des femmes, les leçons de l'explosion
à Jarrie, les chiffres méconnus des armes à feu en Isère, la pénurie de sang, etc.

Vous trouverez dans la revue de presse de `novembre 2022 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-novembre>`_ le nombre de
vegans dans l'agglo, les menaces pesant sur 20 000 arbres à Grenoble,
la fin du garage Solidarauto, la face cachée des livreurs à vélo, etc.

Nous vous recommandons enfin la `revue de presse d'octobre 2022 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-doctobre>`_,
(https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-doctobre)
sur les menaces sur l'eau potable à Grenoble, la bombe sous le pont de Brignoud,
les chiffres de la "Gremorra", les intox sur les fraudes à la CAF Isère, etc.



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_10.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 06h00 https://www.ici-grenoble.org/evenement/le-10-juin-1961-la-naissance-du-planning-familial-de-grenoble
- 14h00 https://www.ici-grenoble.org/evenement/discussion-sur-la-contraception-testiculaire-et-fabrication-de-sous-vetements-contraceptifs
- 14h00 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- |important| |solidarite_ukraine| 15h00 https://www.ici-grenoble.org/evenement/manifestation-de-soutien-au-peuple-ukrainien
- |important| 19h00 https://www.ici-grenoble.org/evenement/resto-malap-repas-congolais-vegan-cuisine-par-des-personnes-sans-papiers
- 19h30 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
- 21h30 🎥 https://www.ici-grenoble.org/evenement/cinema-marinette-sur-la-footballeuse-lesbienne-marinette-pichon-une-heroine-meconnue
- 23h30 https://www.ici-grenoble.org/evenement/glitter-bloc-nuit-de-concerts-pour-les-4-ans-de-lassociation-serein-e-s


|tamis| Événements du Tamis
================================

- https://www.le-tamis.info/evenements


- 10h30 https://www.le-tamis.info/evenement/matchs-de-roller-derby-10-juin-2023


|solidarite_ukraine| Solidarité Ukraine
==========================================

- https://www.ici-grenoble.org/evenement/manifestation-de-soutien-au-peuple-ukrainien
- https://commons.com.ua/en/
- https://ukraine.frama.io/luttes/
- https://ukraine.frama.io/media-2023/brigades-editoriales-de-solidarite/brigades-editoriales-de-solidarite.html
- https://ukraine.frama.io/media-2022/brigades-editoriales-de-solidarite/brigades-editoriales-de-solidarite.html

Soutien à l’Ukraine résistante N°20 juin 2023
--------------------------------------------------

- https://ukraine.frama.io/media-2023/_downloads/d33cb8f6210341e43353fc1d4a2eed37/20_soutien-a-l-ukraine-resistante_2023_06_01.pdf

"""L’un des paradoxes de la guerre en Ukraine est que certains d’entre nous ont découvert
l’existence d’une gauche active et d’une pensée critique et créative en
Ukraine que nous avons ignorées pendant de trop nombreuses années (et dont
l’auteur de ces lignes fait partie).
Parmi nos révélations, `le journal Commons <https://commons.com.ua/en/>`_, journal de critique sociale,
est certainement l’un des endroits les plus importants et les plus productifs
pour comprendre la situation en Ukraine (et dans le monde)"""


|ici_grenoble|  🚧 IA : ici Grenoble lance CamilleGPT 🤣
================================================================

- https://www.ici-grenoble.org/article/ia-ici-grenoble-lance-camillegpt

**Le site internet d'ici Grenoble va bientôt changer** : nous sommes en train
de finaliser une version 2.1 qui nous l'espérons vous plaira.

Parmi les grandes nouveautés : un algorithme copyleft adaptant l'agenda
et les actualités visibles en fonction de vos préférences politiques,
une fonctionnalité Métavers permettant de créer votre propre avatar militant
pour manifester virtuellement dans les rues de Grenoble (plus de 150 skins
disponibles), un TikTok Converter capable de transformer chaque article
du site en une courte vidéo de 7 secondes (avec une chorégraphie), et
plein d'autres surprises grâce à CamilleGPT, l'IA libertaire créée par
notre équipe d'informaticiens à la pointe des logiciels libres.

Meuh non, on blague !!! Le nouveau site d'ici Grenoble restera 0% algorithmé,
0% publicité, 100% intelligence réelle (enfin, euh, pour l'intelligence, on essaye).

**On espère juste que la nouvelle ergonomie et les nouvelles fonctionnalités
vous plairont.**

Lancement dans quelques semaines...**

On espère surtout qu'à chaque fois que vous vous connecterez sur ici Grenoble,
vous trouverez une info surprenante ou stimulante, dans l'agenda, dans
les actualités ou dans le guide des alternatives.

**Un grand merci pour tous les encouragements reçus cette année et le succès
de fréquentation du site** 🙏

Vous êtes des milliers à vous informer chaque semaine sur ici Grenoble !

Belle journée à tou-te-s
L'équipe d'ici Grenoble




