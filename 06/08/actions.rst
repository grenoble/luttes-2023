.. index::
   pair: Luttes; Congo-Brazzaville (2023-06-08)
   pair: Grevue de presse ; 2023-06-08

.. _actions_2023_06_08:

=========================================================================================================================================
**Actions du jeudi 8 juin 2023 #Grenoble #Congo-Brazzaville #GrevueDePresseJuin2023**
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html


#2023-06-08 #Grenoble #Congo-Brazzaville #GrevueDePresseJuin2023


|ici_grenoble| A la une **Les 12 infos de juin**
==================================================================================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-12-infos-de-juin

.. figure:: images/page_une_ici_grenoble_2023_06_08.png
   :align: center

   https://www.ici-grenoble.org/article/grevue-de-presse-les-12-infos-de-juin, https://www.ici-grenoble.org/newsletter


Vous ne lisez pas le Dauphiné Libéré, mais les infos marquantes vous intéressent ?

Chaque mois, le média ici Grenoble vous propose la `Grevue de presse <https://www.ici-grenoble.org/article/grevue-de-presse-les-12-infos-de-juin>`_,
une sélection d'infos courtes et percutantes glanées dans la presse locale
ou dans nos réseaux.

Au menu de juin : un gendarme néonazi en Isère, la clinique mutualiste
sous tutelle, un congé menstruel à Seyssinet, des Cacatovs à Grenoble,
le relogement de Solidarité Femmes Miléna, un appel du Tichodrome, la
nappe toxique sous Grenoble...

Bonne lecture !

|solidarite| Soutien à yovan et mathieu
-----------------------------------------------

- https://www.ici-grenoble.org/article/repression-soutenir-yovan-et-mathieu

**En terroriser deux pour les terroriser tous** ...

Lors de la manifestation du 1er mai 2023 à Grenoble, Yovan et Mathieu ont été arrêtés, placés en
détention provisoire à Varces puis condamnés à 5 mois de prison ferme.

Pourquoi ? Que s'est-il réellement passé pendant cette manifestation ?
En quoi est-ce un tournant dans la répression à Grenoble ?

Pour un autre récit que celui diffusé dans le Dauphiné Libéré, nous vous
recommandons l'analyse et les témoignages diffusés par |car38|  le Collectif
Anti-Répression Isère : `Tribunal politique, où va la République ? <https://www.ici-grenoble.org/article/repression-soutenir-yovan-et-mathieu>`_


Cette revue de presse vous a intéressé ?
-----------------------------------------------

N'hésitez pas à consulter la `revue de presse de mai 2023 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mai>`_
(https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mai)
sur un projet de mégabassine dans le Trièves, des mégapompeurs dans le Grésivaudan,
un drone russe "made in Isère", la clinique mutualiste dans une pyramide
de Ponzi, la fin de la BAF, le début du BOCAL et plein d'autres infos.

Nous vous recommandons également la `revue de presse de mars 2023 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars>`_,
(https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars)
sur le congé menstruel à Grenoble, le point sur la ZFE, le boum du vélo, la
guerre des puces électroniques, les sabotages contre la 5G et plein
d'autres infos.

Nous vous recommandons aussi la `revue de presse de février 2023 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-fevrier>`_,
(https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-fevrier)
sur la nappe phréatique toxique sous Grenoble, le boum des maisons secondaires,
le lancement de La Nébuleuse, l'expulsion du Chantier de Fontaine, etc.

Nous vous recommandons enfin la `revue de presse de janvier 2023 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-janvier>`_,
(https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-janvier)
sur les sols dioxinés de Pont-de-Claix et de Jarrie, l'algorithme secret de
la CAF Isère, le vote du Métrocâble, les stages d'autodéfense féministe,
le bilan de 5 ans de collapsologie à Grenoble, etc.

Vous trouverez par ailleurs dans la `revue de presse de décembre 2022 <https://www.ici-grenoble.org/article/grevue-de-presse-les-8-infos-de-decembre>`_
(https://www.ici-grenoble.org/article/grevue-de-presse-les-8-infos-de-decembre)
plein d'infos sur la nouvelle Maison des femmes, les leçons de l'explosion
à Jarrie, les chiffres méconnus des armes à feu en Isère, la pénurie de sang, etc.

Vous trouverez dans la revue de presse de `novembre 2022 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-novembre>`_ le nombre de
vegans dans l'agglo, les menaces pesant sur 20 000 arbres à Grenoble,
la fin du garage Solidarauto, la face cachée des livreurs à vélo, etc.

Nous vous recommandons enfin la `revue de presse d'octobre 2022 <https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-doctobre>`_,
(https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-doctobre)
sur les menaces sur l'eau potable à Grenoble, la bombe sous le pont de Brignoud,
les chiffres de la "Gremorra", les intox sur les fraudes à la CAF Isère, etc.


|ici_grenoble| **Agenda ici-grenoble**
===========================================

.. figure:: images/ici_grenoble_2023_06_08.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 09h58 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 10h00 https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- 12h00 https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- 14h00 https://www.ici-grenoble.org/evenement/permanence-pour-les-personnes-victimes-ou-temoins-de-discriminations-racistes-sexistes-lgbtqiphobes
- 14h30 https://www.ici-grenoble.org/evenement/permanence-dautodefense-pour-lacces-aux-droits-caf-secu-titres-de-sejour-dossiers-a-remplir
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- 18h00 https://www.ici-grenoble.org/evenement/atelier-de-francais-a-prix-libre-pour-debutant-es-et-intermediaires
- 18h00 https://www.ici-grenoble.org/evenement/rencontre-avec-lauteur-du-livre-redonner-du-sens-au-travail-une-aspiration-revolutionnaire
- |important| 18h30 https://www.ici-grenoble.org/evenement/discussion-la-francafrique-au-congo-brazaville
- 19h00 🎥 https://www.ici-grenoble.org/evenement/cine-debat-tous-au-larzac-sur-les-luttes-contre-lextension-du-camp-militaire-du-larzac-de-1970-a-1981
- |important| 21h00 https://www.ici-grenoble.org/evenement/concert-de-mussi-kongo-artiste-engage-du-congo-brazzaville, https://fr.wikipedia.org/wiki/R%C3%A9publique_du_Congo
- 21h30 🎥 https://www.ici-grenoble.org/evenement/cinema-marinette-sur-la-footballeuse-lesbienne-marinette-pichon-une-heroine-meconnue

|tamis| Événements du Tamis
================================

- https://www.le-tamis.info/evenements


- 19h00 https://www.le-tamis.info/evenement/jaccueille-une-personne-refugiee-reunion-dinformat

