
.. _actions_2023_06_21:

=========================================================================================================================================
**Actions du mercredi 21 juin 2023 #Grenoble #SoulevementsDeLaTerre**
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html

#2023-06-20 #Grenoble #SoulevementsDeLaTerre



|ici_grenoble| A la une **Nos luttes sont insolubles**
==================================================================================

- https://www.ici-grenoble.org/article/nos-luttes-sont-insolubles

.. figure:: images/page_une_ici_grenoble_2023_06_21.png
   :align: center

   https://www.ici-grenoble.org/article/nos-luttes-sont-insolubles, https://www.ici-grenoble.org/newsletter



Ce mercredi 21 juin à 20h30 au Jardin Hoche (devant le terrain de basket),
les Soulèvements de la Terre et Extinction Rebellion Grenoble organisent
une fanfare/parade contre la dissolution des Soulèvements de la Terre
annoncée par le gouvernement.

Voici le communiqué :

"La première menace de dissolution de Gérald Darmanin le 28 mars dernier
a provoqué une vague de soutiens inédite : plus de 100 000 personnes
se sont revendiquées des Soulèvements de la Terre dans un appel qui
continue de se renforcer.

Dans le même temps, partout en France et à l'étranger ont fleuri plus de 170 comités locaux.

Aujourd'hui, mercredi 21 juin, le gouvernement prononce la dissolution
effective des Soulèvements de la Terre, que nous dénonçons comme une
attaque grave contre les libertés fondamentales et la démocratie.

Cette dissolution marque un précédent dangereux, un point de non-retour
dans la dérive autoritaire du gouvernement. Décrié par l'ONU pour son
maintien de l'ordre brutal, ses violences policières, sa criminalisation
du militantisme écologique, le gouvernement a pour seule réponse une
fuite en avant répressive.

Après avoir court-circuité la représentation nationale à coup de 49.3 et
ignoré des millions de manifestants lors de sa réforme des retraites,
il rend illégal un mouvement soutenu par une centaine de milliers de
citoyennes et citoyens.

Alors que l'urgence climatique et écologique se fait chaque jour plus
pressante, que les sécheresses et incendies s'intensifient, que la
biodiversité s'effondre, le gouvernement poursuit son œuvre écocidaire,
au service des lobbies du BTP, de l'agrochimie et de la finance.

En témoigne l'appel d'Emmanuel Macron à une « pause réglementaire sur
les normes environnementales européennes », comme si la catastrophe
pouvait attendre.

Ces manœuvres sont celles d’un vieux monde, condamné mais incapable de changer de direction.

Face à un capitalisme frénétique dans son accaparement de la terre, de
l'eau et des ressources, les mouvements de défense du vivant sont taxés
d'écoterroristes.

La criminalisation des oppositions n'est que la stratégie classique
d'un pouvoir aux abois.

Ce que le gouvernement feint d'ignorer, c’est que la dissolution des
Soulèvements de la Terre ne suffira pas. Un mouvement qui se bat pour la
protection des biens communs que sont l'eau et la terre, ne peut être dissout.

Partout nous refleurirons car la répression brutale imposée par le
gouvernement reste toujours moins effrayante que l'avenir suffoquant
qu'il nous réserve.

Soyons nombreuses et nombreux à manifester notre soutien aux Soulèvements
de la Terre, rejoignons les appels des comités locaux, et notamment celui
de Grenoble, dès aujourd’hui, 21 juin, lors de la fête de la musique !

Invitons tout⋅es nos camarades artistes, programmateur⋅ices de concerts,
technicien⋅nes sons et lumière, etc à manifester leur soutien au mouvement
lors lors des concerts.

Donnons-nous rendez-vous collectivement la semaine prochaine,
mercredi 28 juin à 18h devant la préfecture de l’Isère, place de Verdun
pour dénoncer cette dangereuse dissolution par un soulèvement festif !"

Contact : sdt-grenoble@proton.me


**Agenda ici-grenoble**
==========================

.. figure::  images/ici_grenoble_2023_06_21.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 09h56 https://www.ici-grenoble.org/evenement/proces-waffenkraft-impliquant-un-gendarme-neonazi-iserois
- 14h00 https://www.ici-grenoble.org/evenement/cafe-chomeur-euse-s-echange-dinfos-et-dexperiences-hors-du-monde-du-travail
- 15h00 https://www.ici-grenoble.org/evenement/permanence-du-lokal-autogere
- 17h10 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
- 18h30 https://www.ici-grenoble.org/evenement/defaite-de-la-musique-2023
- 20h30 https://www.ici-grenoble.org/evenement/fanfare-parade-contre-la-dissolution-des-soulevements-de-la-terre
- 22h00 https://www.ici-grenoble.org/evenement/special-retraites-pourquoi-lintersyndicale-a-echoue-jusquici-selon-frederic-lordon



2023-06-21 Fanfare-parade contre la dissolution des Soulèvements de la Terre
==================================================================================

- https://www.ici-grenoble.org/evenement/fanfare-parade-contre-la-dissolution-des-soulevements-de-la-terre
- https://www.ldh-france.org/la-dissolution-des-soulevements-de-la-terre-vise-a-faire-taire-la-contestation-politique/
- https://www.lemonde.fr/planete/article/2023/06/21/la-dissolution-du-collectif-ecologiste-les-soulevements-de-la-terre-a-ete-prononcee_6178559_3244.html
- https://umap.openstreetmap.fr/fr/map/21-juin-28-juin-rassemblements-contre-la-repressio_931019#7/47.190/-2.999
- https://librinfo74.fr/un-pied-de-nez-a-darmanin-les-soulevements-de-la-terre-fetent-la-dissolution-en-musique/


Ce mercredi 21 juin à 20h30 au Jardin Hoche (devant le terrain de basket),
les Soulèvements de la Terre et Extinction Rebellion Grenoble organisent
une fanfare/parade contre la dissolution des Soulèvements de la Terre
annoncée par le gouvernement.

Habits bleus bienvenus. Instrument de musique bienvenues.
Corde vocale chaudes patates plus que bienvenues.

Voici le communiqué :

"La première menace de dissolution de Gérald Darmanin le 28 mars dernier
a provoqué une vague de soutiens inédite : plus de 100 000 personnes se
sont revendiquées des Soulèvements de la Terre dans un appel qui continue
de se renforcer.

Dans le même temps, partout en France et à l'étranger ont fleuri plus de
170 comités locaux.

Aujourd'hui, mercredi 21 juin, le gouvernement prononce la dissolution
effective des Soulèvements de la Terre, que nous dénonçons comme une
attaque grave contre les libertés fondamentales et la démocratie.

Cette dissolution marque un précédent dangereux, un point de non-retour
dans la dérive autoritaire du gouvernement.

Décrié par l'ONU pour son maintien de l'ordre brutal, ses violences policières,
sa criminalisation du militantisme écologique, le gouvernement a pour
seule réponse une fuite en avant répressive.

Après avoir court-circuité la représentation nationale à coup de 49.3 et
ignoré des millions de manifestants lors de sa réforme des retraites,
il rend illégal un mouvement soutenu par une centaine de milliers de
citoyennes et citoyens.

Alors que l'urgence climatique et écologique se fait chaque jour plus
pressante, que les sécheresses et incendies s'intensifient, que la
biodiversité s'effondre, le gouvernement poursuit son œuvre écocidaire,
au service des lobbies du BTP, de l'agrochimie et de la finance.

En témoigne l'appel d'Emmanuel Macron à une « pause réglementaire sur
les normes environnementales européennes », comme si la catastrophe
pouvait attendre.

Ces manœuvres sont celles d’un vieux monde, condamné mais incapable de
changer de direction.

Face à un capitalisme frénétique dans son accaparement de la terre, de
l'eau et des ressources, les mouvements de défense du vivant sont taxés
d'écoterroristes.

La criminalisation des oppositions n'est que la stratégie classique
d'un pouvoir aux abois.

Ce que le gouvernement feint d'ignorer, c’est que la dissolution des
Soulèvements de la Terre ne suffira pas.

Un mouvement qui se bat pour la protection des biens communs que sont
l'eau et la terre, ne peut être dissout.

Partout nous refleurirons car la répression brutale imposée par le
gouvernement reste toujours moins effrayante que l'avenir suffoquant
qu'il nous réserve.

Soyons nombreuses et nombreux à manifester notre soutien aux
Soulèvements de la Terre, rejoignons les appels des comités locaux,
et notamment celui de Grenoble, dès aujourd’hui, 21 juin, lors de la
fête de la musique !

Invitons tout⋅es nos camarades artistes, programmateur⋅ices de concerts,
technicien⋅nes sons et lumière, etc à manifester leur soutien au mouvement
lors lors des concerts.

Donnons-nous rendez-vous collectivement la semaine prochaine, mercredi 28 juin
à 18h devant la préfecture de l’Isère, place de Verdun pour dénoncer
cette dangereuse dissolution par un soulèvement festif !"

Contact : sdt-grenoble@proton.me


Article mediapart
=====================

:download:`pdfs/Projet_WaffenKraft_La_France_n_est_pas_a_labri_du_terrorisme_d_ultra-droite_Mediapart.pdf`


