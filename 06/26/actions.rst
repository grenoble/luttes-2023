


=====================
2023-06-26 actions
=====================


2023-06-26 Conf'allant vert " Ravages écologiques et climatiques : les raisons de l'inaction"
==================================================================================================


- https://designfactory.univ-grenoble-alpes.fr/francais/conf-allant-vert-ravages-ecologiques-et-climatiques-les-raisons-de-l-inaction--1233064.kjsp?RH=3121420986300607


Pourquoi les ravages écologiques, causés par les activités humaines,
continuent-ils alors qu’on sait qu’ils menacent à très court terme
l’habitabilité de la Terre pour l’espèce humaine ?

Qu’est-ce qui empêche nos sociétés, nos dirigeants, mais aussi nos proches
et nous même d’en prendre acte et de mettre en œuvre les changements
nécessaires, au lieu de continuer un « business as usual » suicidaire ?

Plutôt que d’invoquer une prétendue « nature humaine » qui serait écocidaire,
l’enquête sociologique permet d’identifier les résistances au changement
dans nos comportements quotidiens.

Un scientifique et une artiste mêlent leurs pratiques pour inviter le
public à passer à l’action.

Charlotte Couturier est une artiste scénique et vocale et formatrice,
expatriée poétique en Belgique depuis 2006.
Explorant les liens entre écriture et oralisation, ses créations ont
également toujours été motivées par l’élan de visibiliser les paroles
des femmes sur scène.

Jean-Michel Hupé est chercheur au CNRS à Toulouse. Spécialiste en
neurosciences, il s’est réorienté depuis cinq ans vers l’écologie
politique avec l’objectif, de partager les savoirs entre disciplines
scientifiques et avec tous les publics afin de lutter contre les causes
des ravages écologiques.

