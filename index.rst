
.. raw:: html

   <a rel="me" href="https://kolektiva.social/@grenobleluttes"></a>


.. ♀️✊ ⚖️ 📣 🙏
.. ✊🏻✊🏼✊🏽✊🏾✊🏿
.. 🤥
.. 🤪
.. ⚖️ 👨‍🎓
.. 🔥
.. 💧
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳
.. 🇫🇷
.. 🇧🇪
.. 🇺🇦
.. 🇬🇧
.. 🇨🇭
.. 🇩🇪
.. 🌍 ♀️✊🏽
.. 🇮🇷
.. 🎥 🎦
.. ⏚

.. un·e

|FluxWeb| `RSS <https://grenoble.frama.io/luttes-2023/rss.xml>`_

.. _luttes_grenoble_2023:

=========================================
**Luttes Grenoble 2023**
=========================================

#NoBassaran #JeMeSoulève #Grenoble

:ref:`grenoble_infos:agendas_grenoble`


.. toctree::
   :maxdepth: 6

   07/07
   06/06
   news/news
