.. index::
   ! Nahel

.. _actions_2023_07_01:

=========================================================================================================================================
|sdterre| **Actions du samedi 1er juillet 2023 #Grenoble #Solidarité #AntiRacisme #Nahel**
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html

#2023-06-28 #Grenoble #Solidarité #SoulevementsDeLaTerre



|ici_grenoble|  A la une **Justice pour Nahel, Camara et...**
==================================================================================

- https://www.ici-grenoble.org/article/justice-pour-nahel-camara-et
- https://www.ici-grenoble.org/structure/le-front-uni-des-immigrations-et-des-quartiers-populaires-de-grenoble-fuiqp

.. figure:: images/page_une_ici_grenoble_2023_07_01.png
   :align: center

   https://www.ici-grenoble.org/article/justice-pour-nahel-camara-et


Introduction
-----------------

Vendredi 30 juin 2023 à 20h place Verdun, le `FUIQP Grenoble <https://www.ici-grenoble.org/structure/le-front-uni-des-immigrations-et-des-quartiers-populaires-de-grenoble-fuiqp>`_ appelle à un
rassemblement pour réclamer Justice pour Nahel, Justice pour Al hussein
Camara et pour tou-te-s les autres.

Voici l'appel :

"Une nouvelle fois nous sommes en deuil et en colère dans les quartiers
populaires suite à l’assassinat d’un petit frère par un policier.

Apres Angoulême avec l'assassinat de Camara, cette fois c’est à Nanterre
que le jeune Nahel âgé d’à peine 17 ans est abattu de sang-froid lors
d’un contrôle de police comme le prouve une vidéo diffusée sur Twitter
éliminant l’hypothèse de la légitime défense policière.

Nous apportons notre soutien aux familles et nous disons stop aux
violences policières !!"

À 20h

Place Verdun
Grenoble



**Agenda ici-grenoble**
==========================

.. figure::  images/ici_grenoble_2023_07_01.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 06h00 https://www.ici-grenoble.org/evenement/imposition-dune-zfe-injuste-et-inefficace

  - https://reporterre.net/Les-zones-a-faibles-emissions-ZFE-sont-elles-vraiment-ecolos

- |important| 08h00 https://www.ici-grenoble.org/evenement/journee-des-enfants-afrodescendants-ateliers-conference-repas-africain
- 14h45 https://www.ici-grenoble.org/evenement/cinema-la-sirene-sur-la-guerre-oubliee-iran-irak
- 15h00 https://www.ici-grenoble.org/evenement/marche-des-fiertes-de-grenoble
- 18h00 https://www.ici-grenoble.org/evenement/boum-queer-after-pride-volume-1
- 21h00 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
- 22h00 https://www.ici-grenoble.org/evenement/boum-queer-after-pride-volume-2

  - https://grenoble-fiertes.com/

