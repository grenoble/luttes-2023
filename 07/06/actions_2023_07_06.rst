
.. _actions_2023_07_06:

=========================================================================================================================================
**Actions du jeudi 6 juillet 2023 #Grenoble #Anticapitalisme #DesastreClimatique #Antifascisme**
=========================================================================================================================================

- https://grenoble.frama.io/infos/agendas/agendas.html

#2023-07-06 #Grenoble #Anticapitalisme #DesastreClimatique #Antifascisme


|ici_grenoble|  A la une **L'AG écolo anticapitaliste**
==================================================================================

- https://www.ici-grenoble.org/article/lag-ecolo-anticapitaliste

.. figure:: images/page_une_ici_grenoble_2023_07_06.png
   :align: center

   https://www.ici-grenoble.org/article/lag-ecolo-anticapitaliste

Introduction
-----------------

Face au désastre climatique, vous pensez qu'il ne reste que deux scénarios
possibles : l'aggravation ou le renversement du système capitaliste ?

Ce jeudi 6 juillet à 19h au 38 rue d'Alembert, c'est l'Assemblée Générale
Écolo Anticapitaliste de Grenoble.

Qui l'organise ? Dans quel but concret ?

Voici l'invitation
-------------------------

"Cette assemblée générale a vu le jour à l'occasion d'une manifestation
d'opposition à la mascarade Grenoble Capitale Verte européenne 2022.

Nous sommes composé·es d'organisations, de collectifs, de partis politiques,
mais aussi d'individus, qui ensemble, souhaitons porter des revendications
écologiques anticapitalistes.

L'écologie et l'anticapitalisme sont pour nous indissociables : le capitalisme
se fonde sur l'accumulation, et donc l'exploitation, de ressources qui
se font de plus en plus rares, au profit d'entreprises ou de patrons qui
s'enrichissent de plus en plus sur le dos des travailleur·euses et des
personnes précaires.

Il est impossible de croire en un capitalisme vert pour nous sauver de
la crise écologique : les solutions techniques cherchent en réalité à
ouvrir de nouveaux marchés lucratifs sans remettre en question le principe-même
du capitalisme qui est à la racine des problèmes environnementaux.

Nous avons besoins de retrouver une autonomie et une souveraineté
collective pour pouvoir faire des choix de société cohérents avec les
limites planétaires.
Or cela est inconcevable si les décisions économiques et politiques sont
prises par une classe dont les intérêts divergent de ceux du bien commun.

Combattre le capitalisme c'est aussi faire la critique d'une idéologie
propagée par les classes dominantes qui promeut la consommation et un
mode de vie débridé comme marqueurs de réussite sociale.

Nous souhaitons lutter contre cette vision du monde et ces normes qui
doivent êtres changées pour aller vers une sobriété qui ne soit pas subie
mais désirée car profitable à tout le monde.

Nous avons fait le choix de nous organiser en AG pour permettre à des
individus aussi bien qu'à des personnes mandatées par leur collectif de
rejoindre l'organisation.

Ce cadre unitaire nous permet d'avoir plus de force pour mener cette lutte
transversale, et de rattacher et mêler des causes diverses qui ne se
croisent pas facilement dans d'autres espaces.

Le but n'est pas de remplacer les personnes déjà en lutte mais d'être un
outil de soutien et de renforcement. Nos opinions peuvent diverger mais
nous nous rejoignons sur ces revendications que nous souhaitons porter
à chacune de nos apparitions publiques.

Nous revendiquons :

- un accès mutualisé et inconditionnel à tous les besoins vitaux pour tous·tes.
  Ce sont les plus riches qui polluent et consomment le plus, impactant
  les personnes les plus précaires qui se retrouvent privées des ressources
  nécessaires à leur survie (eau, nourriture, logement, santé, transport, etc.).
- des transports en commun gratuits et la suppression des ZFE qui
  précarisent encore plus
- la réquisition des logements vides et la rénovation des logements
  sans hausse de loyer, notamment les moins bien isolés
- la fin de l’artificialisation des sols
- la fin des grands projets urbains écologiquement condamnables qui ne
  répondent qu'à des logiques de croissance et de rayonnement économiques.
  D'autant plus quand ces projets s'accompagnent d'une gentrification des quartiers.

Pour cela, nos moyens d'action sont :

- le renforcement et le soutien des luttes locales
- la construction d'un cadre unitaire large d'organisations, d'individus,
  de collectifs qui permet de partager, échanger, et construire des
  revendications communes et unies
- l'apparition lors de manifestations ou l'action conjointe pour
  construire un rapport de forces sur le territoire
- le relai d'informations

Rejoins-nous et viens nous aider à construire la lutte !"

À 18h30

Au Centre Social « Tchoukar »
38 rue d'Alembert
Grenoble





**Agenda ici-grenoble**
==========================

.. figure::  images/ici_grenoble_2023_07_06.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 09h59 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 14h10 https://www.ici-grenoble.org/evenement/cinema-la-sirene-sur-la-guerre-oubliee-iran-irak
- 18h25 https://www.ici-grenoble.org/evenement/cinema-marinette-sur-la-footballeuse-lesbienne-marinette-pichon-une-heroine-meconnue
- 18h30 https://www.ici-grenoble.org/evenement/aperotiba-soiree-de-presentation-dalternatiba-grenoble

  - https://grenoble.alternatiba.eu/

- 18h30 https://www.ici-grenoble.org/evenement/cercle-de-lectures-feministes-de-grenoble
- |important| 19h00 https://www.ici-grenoble.org/evenement/ag-ecolo-anticapitaliste
- 21h15 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
