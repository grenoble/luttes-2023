.. index::
   pair: Python ; EARTH GROUND
   pair: Unicode ; EARTH GROUND


.. _news_2023_06:

=====================
2023-06
=====================


2023-02-21 ⏚ #SDLT #SoulevementsdeLaTerre #Unicode Mise à la terre ⏚ #EARTH GROUND #U+23DA
==============================================================================================

- https://www.fileformat.info/info/unicode/char/23da/index.htm
- https://www.fileformat.info/info/unicode/version/4.1/index.htm (2005)

.. figure:: images/unicode_4_1_2005_03.png
   :align: center

Unicode Data
---------------

.. figure:: images/earth_ground_unicode.png
   :align: center

::

    Unicode Data
    Name 	EARTH GROUND
    Block 	Miscellaneous Technical
    Category 	Symbol, Other [So]
    Combine 	0
    BIDI 	Other Neutrals [ON]
    Mirror 	N
    Index entries 	EARTH GROUND
    GROUND, EARTH
    Version 	Unicode 4.1.0 (March 2005)


Encodings
------------

.. figure:: images/earth_ground_encodings.png
   :align: center


::

    HTML Entity (decimal) 	&#9178;
    HTML Entity (hex) 	&#x23da;
    How to type in Microsoft Windows 	Alt +23DA
    UTF-8 (hex) 	0xE2 0x8F 0x9A (e28f9a)
    UTF-8 (binary) 	11100010:10001111:10011010
    UTF-16 (hex) 	0x23DA (23da)
    UTF-16 (decimal) 	9 178
    UTF-32 (hex) 	0x000023DA (23da)
    UTF-32 (decimal) 	9 178
    C/C++/Java source code 	"\u23DA"
    Python source code 	u"\u23DA"


Python code
------------

- https://discuss.afpy.org/t/python-et-unicode/1600/8

.. code-block:: python

   import unicodedata
   unicodedata.name(unicodedata.normalize("NFD", "⏚"))

::


   'EARTH GROUND'


.. code-block:: python

   earth_ground = unicodedata.lookup("EARTH GROUND")

::

    '⏚'


::

    >>> for c in "SDLT ⏚":
    ...    print(unicodedata.name(c))
    ...


    LATIN CAPITAL LETTER S
    LATIN CAPITAL LETTER D
    LATIN CAPITAL LETTER L
    LATIN CAPITAL LETTER T
    SPACE
    EARTH GROUND

